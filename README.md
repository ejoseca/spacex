
Logging
-------

Since logging is not important in this application, the Android's static class Log is used instead of using a logging interface.



Pending features
----------------

- Reload pending requests when network connection is recovered.

- Change rocket launches pagination in the view model to avoid duplicated items.

- Disable interface when "isGointToVideo" is true.

- Give a lifecycle owner to Master List item view holders. Currently, data is static and is loaded before displaying it, it's not a problem.
