package es.joseca.spacex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import dagger.hilt.android.AndroidEntryPoint
import es.joseca.spacex.ui.RocketLaunchesActivity
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var spaceXContext: SpaceXContext

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        findViewById<Button>(R.id.use_iframe_button).setOnClickListener {
            spaceXContext.rocketLaunchVideoSettings.useOfficialApi = false
            startActivity(Intent(this, RocketLaunchesActivity::class.java))
        }

        findViewById<Button>(R.id.use_official_video_button).setOnClickListener {
            spaceXContext.rocketLaunchVideoSettings.useOfficialApi = true
            startActivity(Intent(this, RocketLaunchesActivity::class.java))
        }
    }
}