package es.joseca.spacex

import android.app.Application
import android.content.res.Configuration
import android.util.Log
import android.widget.Toast
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class SpaceXApplication : Application() {

    @Inject
    lateinit var spaceXContext: SpaceXContext

    override fun onCreate() {
        super.onCreate()
        setSpaceXContext(resources.configuration)
        Thread.setDefaultUncaughtExceptionHandler(GlobalExceptionHandler())
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        updateSpaceXContext(newConfig)
    }

    private fun setSpaceXContext(configuration: Configuration) {
        spaceXContext.rocketLaunchesSettings.launchesPerPage = 20
        spaceXContext.rocketLaunchVideoSettings.useOfficialApi = false
        spaceXContext.rocketLaunchVideoSettings.youtubeApiKey = BuildConfig.YOUTUBE_API_KEY
        updateSpaceXContext(configuration)
    }

    private fun updateSpaceXContext(configuration: Configuration) {
        val isTablet = configuration.smallestScreenWidthDp >= 600
        spaceXContext.rocketLaunchesSettings.landscapeLayout = isTablet
        spaceXContext.rocketLaunchesSettings.navigateToDetail = !isTablet
    }

    private inner class GlobalExceptionHandler : Thread.UncaughtExceptionHandler {

        override fun uncaughtException(thread: Thread, throwable: Throwable) {
            Log.e("UncaughtException", throwable.stackTraceToString(), throwable)
            // For debugging purposes, we are just displaying a message when an error happens
            Toast.makeText(this@SpaceXApplication, throwable.message, Toast.LENGTH_SHORT).show()
        }
    }
}