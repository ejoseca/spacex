package es.joseca.spacex

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SpaceXContext @Inject constructor() {

    val retrofitServicesSettings = RetrofitServicesSettings()

    class RetrofitServicesSettings {

        var spaceXBaseUrl = "https://api.spacexdata.com/v4/"
    }

    val rocketLaunchesSettings = RocketLaunchesSettings()

    class RocketLaunchesSettings {

        var landscapeLayout = false

        var navigateToDetail = true

        var launchesPerPage = 10
    }

    val rocketLaunchVideoSettings = RocketLaunchVideoSettings()

    class RocketLaunchVideoSettings {

        var useOfficialApi = false

        var youtubeApiKey = ""
    }
}