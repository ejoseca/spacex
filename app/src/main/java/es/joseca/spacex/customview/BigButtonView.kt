package es.joseca.spacex.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.databinding.ObservableField
import kotlin.math.min

class BigButtonView : View {

    val isPressed = object : ObservableField<Boolean>(false) {
        override fun set(value: Boolean) {
            super.set(value)
            invalidate()
            requestLayout()
        }
    }

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val pressed = when (event.action) {
            MotionEvent.ACTION_DOWN -> true
            MotionEvent.ACTION_UP -> false
            MotionEvent.ACTION_CANCEL -> false
            else -> null
        }
        if (pressed != null)
            isPressed.set(pressed)
        return if (pressed != null) true else super.onTouchEvent(event)
    }

    /******************************/
    /*********** Draw *************/
    /******************************/

    private val paint = Paint().apply {
        color = Color.RED
        style = Paint.Style.FILL
        flags = Paint.ANTI_ALIAS_FLAG
    }

    private val bottomCircleRectF = RectF()

    private val topCircleRectF = RectF()

    private val bodyRectF = RectF()

    private val topColor = Color.rgb(255, 0, 0)

    private val bottomColor = Color.rgb(127, 0, 0)

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val body = min(w, h) / 8f
        val perps = body / 2
        val diameter = min(w.toFloat(), h - body)
        val left = (w - diameter) / 2
        val top = (h - diameter - body) / 2
        val right = left + diameter
        val bottom = top + diameter + body
        bottomCircleRectF.set(left, top + body + perps, right, bottom - perps)
        topCircleRectF.set(left, top + perps, right, bottom - body - perps)
        bodyRectF.set(left, top + diameter / 2, right, bottom - diameter / 2)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (isPressed.get()!!) {
            paint.color = topColor
            canvas.drawOval(bottomCircleRectF, paint)
        } else {
            paint.color = bottomColor
            canvas.drawOval(bottomCircleRectF, paint)
            canvas.drawRect(bodyRectF, paint)
            paint.color = topColor
            canvas.drawOval(topCircleRectF, paint)
        }
    }
}