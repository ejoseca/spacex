package es.joseca.spacex.customview

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.animation.addListener


class DataContainer : ConstraintLayout {

    var hasEntered = false
        private set

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val alpha = attrs.getAttributeFloatValue(
                "http://schemas.android.com/apk/res/android",
                "alpha",
                Float.NaN
            )
            if (alpha.isNaN())
                this.alpha = 0f
        } else {
            alpha = 0f
        }
    }

    fun enterWithAnimation(): Animator {
        val heightAnimator = ValueAnimator.ofInt(0, measuredHeight)
        heightAnimator.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            val layoutParams: ViewGroup.LayoutParams = layoutParams
            layoutParams.height = value
            setLayoutParams(layoutParams)
            if (valueAnimator.currentPlayTime > 20)
                alpha = 1f
        }
        heightAnimator.addListener(onEnd = {
            // If the animation is too short, we can set alpha=1 here
            alpha = 1f
        })
        heightAnimator.duration = (measuredHeight / 300.dp() * 1000).toLong()
        heightAnimator.start()
        hasEntered = true
        return heightAnimator
    }

    private fun Int.dp() = this * resources.displayMetrics.density
}