package es.joseca.spacex.customview

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.databinding.ObservableField
import kotlin.math.PI
import kotlin.math.acos
import kotlin.math.min

class GaugeView : View {

    val value = object : ObservableField<Float>(0.25f) {

        override fun set(value: Float) {
            super.set(value.coerceIn(0f, 1f))
            updateRectangles(width, height, get()!!)
            invalidate()
            requestLayout()
        }
    }

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
    }

    /******************************/
    /*********** Draw *************/
    /******************************/

    private val gaugeRectF = RectF()

    private val gaugePath = Path()

    private val chromeRectF = RectF()

    private val gaugeColor = Color.rgb(0, 196, 0)

    private val chromeColor = Color.rgb(127, 127, 0)

    private val gaugePin = PointF()

    private var gaugeDegrees = 0f

    private val gaugePaint = Paint().apply {
        color = gaugeColor
        style = Paint.Style.FILL
        flags = Paint.ANTI_ALIAS_FLAG
    }

    private val chromePaint = Paint().apply {
        color = chromeColor
        style = Paint.Style.STROKE
        flags = Paint.ANTI_ALIAS_FLAG
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateRectangles(w, h, value.get()!!)
    }

    private fun updateRectangles(w: Int, h: Int, value: Float) {
        val padding = min(w / 2f, h.toFloat()) / 20f
        val gaugeWidth = padding * 2
        val radius = min(w / 2f - padding, h - padding)
        chromeRectF.set(
            w / 2f - radius,
            (h - radius - 2 * padding) / 2f + padding,
            w / 2f + radius,
            (h - radius - 2 * padding) / 2f + padding + 2 * radius
        )
        chromePaint.strokeWidth = gaugeWidth
        gaugeRectF.set(
            chromeRectF.left,
            chromeRectF.top + radius - gaugeWidth / 2,
            w / 2f,
            chromeRectF.top + radius + gaugeWidth / 2
        )
        gaugePin.set(
            w / 2f,
            chromeRectF.top + radius
        )
        gaugeDegrees = value * 180
        gaugePath.reset()
        gaugePath.moveTo(gaugeRectF.right, gaugeRectF.top)
        gaugePath.lineTo(gaugeRectF.right, gaugeRectF.bottom)
        gaugePath.lineTo(gaugeRectF.left, (gaugeRectF.top + gaugeRectF.bottom) / 2f)
        gaugePath.close()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawArc(
            chromeRectF.left,
            chromeRectF.top,
            chromeRectF.right,
            chromeRectF.bottom,
            185f,
            170f,
            false,
            chromePaint
        )
        canvas.rotate(gaugeDegrees, gaugePin.x, gaugePin.y)
        canvas.drawPath(gaugePath, gaugePaint)
    }
}