package es.joseca.spacex.customview

import android.animation.ArgbEvaluator
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.FloatProperty
import android.util.TypedValue
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.databinding.Observable
import androidx.dynamicanimation.animation.FloatPropertyCompat
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import es.joseca.spacex.R
import es.joseca.spacex.databinding.RocketLaunchVideoOverlayViewBinding
import es.joseca.spacex.model.service.MysteriousMachine

class RocketLaunchVideoOverlayView : ConstraintLayout {

    companion object {

        @JvmStatic
        @InverseBindingAdapter(attribute = "ratio")
        fun getRatio(view: RocketLaunchVideoOverlayView) = view.binding.ratioSlider.value.get()!!

        @JvmStatic
        @BindingAdapter("app:ratioAttrChanged")
        fun setRatioListener(view: RocketLaunchVideoOverlayView, listener: InverseBindingListener) {
            view.binding.ratioSlider.value.addOnPropertyChangedCallback(object :
                Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                    listener.onChange()
                }
            })
        }

        @JvmStatic
        @InverseBindingAdapter(attribute = "bigButtonPressed")
        fun isBigButtonPressed(view: RocketLaunchVideoOverlayView) =
            view.binding.bigButton.isPressed.get()!!

        @JvmStatic
        @BindingAdapter("app:bigButtonPressedAttrChanged")
        fun setBigButtonPressedListener(
            view: RocketLaunchVideoOverlayView,
            listener: InverseBindingListener
        ) {
            view.binding.bigButton.isPressed.addOnPropertyChangedCallback(object :
                Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                    listener.onChange()
                }
            })
        }
    }

    private lateinit var binding: RocketLaunchVideoOverlayViewBinding

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        binding = RocketLaunchVideoOverlayViewBinding.bind(
            inflate(
                context,
                R.layout.rocket_launch_video_overlay_view,
                this
            )
        )
        binding.magicNumberTextView.text = 0.toString()
        binding.magicNumberTextView.setTextColor(Color.CYAN)
    }

    fun setMagicNumberSpeed(speed: Double) {
        val rate = (speed.toFloat() / 1.5f).coerceIn(0f, 20f)
        val textSize = 16f + rate // Max: 16 + 20 = 36
        binding.magicNumberTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
        binding.magicNumberTextView.setTextColor(
            ArgbEvaluator().evaluate(
                rate / 20f,
                Color.BLUE,
                Color.RED
            ) as Int
        )
    }

    fun setMagicNumber(magicNumber: Double) {
        binding.magicNumberTextView.text = magicNumber.toInt().toString()
    }

    fun setRatio(ratio: Float) {
        if (ratio != binding.ratioSlider.value.get())
            binding.ratioSlider.value.set(ratio)
    }

    fun setBigButtonPressed(isPressed: Boolean) {
        if (isPressed != binding.bigButton.isPressed.get())
            binding.bigButton.isPressed.set(isPressed)
    }

    fun setGauge(value: Double) {
        val min = MysteriousMachine.minGaugeValue
        val max = MysteriousMachine.maxGaugeValue
        binding.gaugeView.value.set(((value - min) / (max - min)).toFloat())
    }

    /****** Signal ******/

    private var signalVelocity = 0f

    fun setSignal(value: Int) {
        val factor = 1000f
        val maxValue = MysteriousMachine.maxSignal * 1.2f
        SpringAnimation(
            binding.signalDrawer,
            FloatPropertyCompat.createFloatPropertyCompat(object :
                FloatProperty<GaugeView>("value") {
                override fun get(gaugeView: GaugeView): Float {
                    return (gaugeView.value.get()!! - 0.5f) * 2 * maxValue * factor
                }

                override fun setValue(gaugeView: GaugeView, value: Float) {
                    gaugeView.value.set(value / factor / maxValue / 2 + 0.5f)
                }
            }),
            value * 1000f
        ).apply {
            spring.stiffness = SpringForce.STIFFNESS_VERY_LOW
            setStartVelocity(signalVelocity)
            addUpdateListener { _, _, velocity ->
                signalVelocity = velocity
            }
        }.start()
    }
}