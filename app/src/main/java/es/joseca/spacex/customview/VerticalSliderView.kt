package es.joseca.spacex.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.databinding.ObservableField

class VerticalSliderView : View {

    val value = object : ObservableField<Float>(0.5f) {

        override fun set(value: Float) {
            super.set(value.coerceIn(0f, 1f))
            updateRectangles(width, height, get()!!)
            invalidate()
            requestLayout()
        }
    }

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
    }


    /******************************/
    /******** Interaction *********/
    /******************************/

    private var isMoving = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val moving = when (event.action) {
            MotionEvent.ACTION_DOWN -> true
            MotionEvent.ACTION_UP -> false
            MotionEvent.ACTION_MOVE -> isMoving
            MotionEvent.ACTION_CANCEL -> null.apply { isMoving = false }
            else -> null
        }
        if (moving != null) {
            isMoving = moving
            val value = pixelToValue(event.y, height)
            this.value.set(value)
        }
        return if (moving != null) true else super.onTouchEvent(event)
    }


    /******************************/
    /*********** Draw *************/
    /******************************/

    private val handlerSide = 36.dp()

    private val paint = Paint().apply {
        color = Color.RED
        style = Paint.Style.FILL
        flags = Paint.ANTI_ALIAS_FLAG
    }

    private val handlerRectF = RectF()

    private val coldRectF = RectF()

    private val hotRectF = RectF()

    private val handlerColor = Color.rgb(196, 196, 196)

    private val coldColor = Color.rgb(0, 0, 255)

    private val hotColor = Color.rgb(255, 0, 0)

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateRectangles(w, h, value.get()!!)
    }

    private fun updateRectangles(w: Int, h: Int, value: Float) {
        val valueHeight = valueToPixel(value, h)
        handlerRectF.set(
            (w - handlerSide) / 2,
            valueHeight - handlerSide / 2,
            (w - handlerSide) / 2 + handlerSide,
            valueHeight + handlerSide / 2
        )
        val barMargin = 8.dp()
        hotRectF.set(
            handlerRectF.left + barMargin,
            valueHeight,
            handlerRectF.right - barMargin,
            h.toFloat() - handlerSide / 8
        )
        coldRectF.set(
            handlerRectF.left + barMargin,
            0f + handlerSide / 8,
            handlerRectF.right - barMargin,
            valueHeight
        )
    }

    private fun valueToPixel(value: Float, height: Int): Float {
        return (1 - value) * (height - handlerSide) + handlerSide / 2
    }

    private fun pixelToValue(pixel: Float, height: Int): Float {
        return 1 - (pixel - handlerSide / 2) / (height - handlerSide)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.color = coldColor
        canvas.drawRect(coldRectF, paint)
        paint.color = hotColor
        canvas.drawRect(hotRectF, paint)
        paint.color = handlerColor
        canvas.drawOval(handlerRectF, paint)
    }

    private fun Int.dp() = this * resources.displayMetrics.density
}