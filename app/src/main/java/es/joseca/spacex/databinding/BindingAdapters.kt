package es.joseca.spacex.databinding

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, imageUrl: String?) {
    loadImage(imageView, imageUrl, null, null)
}


@BindingAdapter(value = ["imageUrl", "placeholder", "fallback"], requireAll = false)
fun loadImage(
    imageView: ImageView,
    imageUrl: String?,
    placeholder: Drawable?,
    fallback: Drawable?
) {
    if (imageUrl?.isEmpty() != false) {
        Glide.with(imageView).load(fallback).into(imageView)
        return
    }
    Glide.with(imageView)
        .load(imageUrl)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .onlyRetrieveFromCache(true)
        .transition(withCrossFade())
        .dontTransform()
        .listener(NoCachedRequestListener(imageView, imageUrl, placeholder, fallback))
        .addListener(FinishedRequestListener(imageView, false))
        .into(imageView)
}

private class NoCachedRequestListener(
    private val imageView: ImageView,
    private val imageUrl: String?,
    private val placeholder: Drawable?,
    private val fallback: Drawable?
) : RequestListener<Drawable> {

    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        imageView.post {
            Glide.with(imageView)
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(placeholder)
                .fallback(fallback)
                .transition(withCrossFade())
                .listener(FinishedRequestListener(imageView, true))
                .into(imageView)
            if (placeholder is AnimationDrawable)
                placeholder.start()
        }
        return true
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        return false
    }
}

private class FinishedRequestListener(val imageView: ImageView, val finishedOnFailure: Boolean) :
    RequestListener<Drawable> {

    @Suppress("UNCHECKED_CAST")
    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        if (finishedOnFailure) {
            try {
                val fragment = FragmentManager.findFragment<Fragment>(imageView)
                return (fragment as? RequestListener<Drawable>)?.onLoadFailed(
                    e,
                    model,
                    target,
                    isFirstResource
                ) ?: false
            } catch (ex: Exception) {
            }
        }
        return false
    }

    @Suppress("UNCHECKED_CAST")
    override fun onResourceReady(
        resource: Drawable,
        model: Any,
        target: Target<Drawable>,
        dataSource: DataSource,
        isFirstResource: Boolean
    ): Boolean {
        try {
            val fragment = FragmentManager.findFragment<Fragment>(imageView)
            return (fragment as? RequestListener<Drawable>)?.onResourceReady(
                resource,
                model,
                target,
                dataSource,
                isFirstResource
            ) ?: false
        } catch (ex: Exception) {
        }
        return false
    }
}