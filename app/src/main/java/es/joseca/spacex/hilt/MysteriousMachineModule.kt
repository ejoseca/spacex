package es.joseca.spacex.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import es.joseca.spacex.model.service.MysteriousMachine
import es.joseca.spacex.model.service.implementation.MysteriousMachineImpl
import kotlin.time.ExperimentalTime

@Module
@InstallIn(SingletonComponent::class)
abstract class MysteriousMachineModule {

    @ExperimentalTime
    @Binds
    abstract fun bindMysteriousMachine(impl: MysteriousMachineImpl): MysteriousMachine
}