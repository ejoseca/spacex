package es.joseca.spacex.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import es.joseca.spacex.model.repository.SpaceXApiRepository
import es.joseca.spacex.model.repository.implementation.SpaceXApiRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindSpaceXApi(impl: SpaceXApiRepositoryImpl): SpaceXApiRepository
}