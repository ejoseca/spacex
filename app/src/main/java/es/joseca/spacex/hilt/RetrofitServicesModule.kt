package es.joseca.spacex.hilt

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.model.service.SpaceXApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
@InstallIn(SingletonComponent::class)
class RetrofitServicesModule {

    @Provides
    fun bindSpaceXApi(spaceXContext: SpaceXContext): SpaceXApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(spaceXContext.retrofitServicesSettings.spaceXBaseUrl)
//            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(SpaceXApiService::class.java)
    }


//    class NullOnEmptyConverterFactory : Converter.Factory() {
//
//        override fun responseBodyConverter(
//            type: Type,
//            annotations: Array<Annotation?>,
//            retrofit: Retrofit
//        ): Converter<ResponseBody, *> {
//            return Converter<ResponseBody, Any?> { value ->
//                if (value.contentLength() == 0L)
//                    null
//                else
//                    retrofit.nextResponseBodyConverter<Any?>(
//                        this@NullOnEmptyConverterFactory,
//                        type,
//                        annotations
//                    ).convert(value)
//            }
//        }
//    }
}