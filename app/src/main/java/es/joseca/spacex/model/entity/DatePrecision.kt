package es.joseca.spacex.model.entity

enum class DatePrecision(val precision: Int) {
    year(1),
    half(2),
    quarter(3),
    month(4),
    day(5),
    hour(6);
}