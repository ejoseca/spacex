package es.joseca.spacex.model.entity

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import es.joseca.spacex.BR
import java.time.Instant

class Launch : BaseObservable() {

    @Bindable
    var id: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.id)
        }

    @Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }

    @Bindable
    var imageUrl: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.imageUrl)
        }

    @Bindable
    var youtubeVideoId: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.youtubeVideoId)
        }

    @Bindable
    var patchUrl: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.patchUrl)
        }

    @Bindable
    var date: Instant = Instant.MAX
        set(value) {
            field = value
            notifyPropertyChanged(BR.date)
        }

    @Bindable
    var datePrecision: DatePrecision = DatePrecision.year
        set(value) {
            field = value
            notifyPropertyChanged(BR.datePrecision)
        }

    @Bindable
    var flightNumber: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.flightNumber)
        }

    @Bindable
    var details: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.details)
        }

    @Bindable
    var rocket: Rocket = Rocket()
        set(value) {
            field = value
            notifyPropertyChanged(BR.rocket)
        }
}