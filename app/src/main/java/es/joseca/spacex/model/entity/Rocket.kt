package es.joseca.spacex.model.entity

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import es.joseca.spacex.BR

class Rocket : BaseObservable() {

    @Bindable
    var id: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.id)
        }

    @Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }
}