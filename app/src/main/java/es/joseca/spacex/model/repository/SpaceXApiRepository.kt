package es.joseca.spacex.model.repository

import es.joseca.spacex.model.entity.Launch
import es.joseca.spacex.model.entity.Rocket

interface SpaceXApiRepository {

    suspend fun getRockets(): List<Rocket>

    suspend fun getPaginatedLaunches(offset: Int, limit: Int): List<Launch>

    suspend fun getPaginatedLaunches(rocket: Rocket, offset: Int, limit: Int): List<Launch>

    suspend fun getRocket(id: String): Rocket?

    suspend fun getLaunch(id: String): Launch?
}