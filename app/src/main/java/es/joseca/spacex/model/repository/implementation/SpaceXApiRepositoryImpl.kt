package es.joseca.spacex.model.repository.implementation

import es.joseca.spacex.model.entity.DatePrecision
import es.joseca.spacex.model.entity.Launch
import es.joseca.spacex.model.entity.Rocket
import es.joseca.spacex.model.repository.SpaceXApiRepository
import es.joseca.spacex.model.service.SpaceXApiService
import java.lang.RuntimeException
import java.time.Duration
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SpaceXApiRepositoryImpl @Inject constructor(private val spaceXApiService: SpaceXApiService) :
    SpaceXApiRepository {

    private val cachedRockets = Cache<Rocket>()

    private val cachedLaunches = Cache<Launch>()

    override suspend fun getRockets(): List<Rocket> {
        val response = spaceXApiService.getRockets()
        if (!response.isSuccessful)
            throw RuntimeException("GetRockets failed with HTTP code ${response.code()}")
        if (response.body() == null)
            throw RuntimeException("SpaceX API returned an invalid value")

        val rockets = mutableListOf<Rocket>()
        for (rocketResponse in response.body()!!) {
            val rocket = Rocket().apply {
                id = rocketResponse.id
                name = rocketResponse.name
            }
            cachedRockets.set(rocket.id, rocket)
            rockets.add(rocket)
        }
        return rockets
    }

    override suspend fun getPaginatedLaunches(offset: Int, limit: Int): List<Launch> {
        return internalGetPaginatedLaunches(null, offset, limit)
    }

    override suspend fun getPaginatedLaunches(
        rocket: Rocket,
        offset: Int,
        limit: Int
    ): List<Launch> {
        return internalGetPaginatedLaunches(rocket, offset, limit)
    }

    private suspend fun internalGetPaginatedLaunches(
        rocket: Rocket?,
        offset: Int,
        limit: Int
    ): List<Launch> {
        val response = spaceXApiService.getLaunches(SpaceXApiService.LaunchQuery().apply {
            query.rocket = rocket?.id
            options.offset = offset
            options.limit = limit
        })
        if (!response.isSuccessful)
            throw RuntimeException("GetLaunches failed with HTTP code ${response.code()}")
        if (response.body() == null)
            throw RuntimeException("SpaceX API returned an invalid value")

        val launches = mutableListOf<Launch>()
        for (launchResponse in response.body()!!.docs) {
            val launch = createLaunchFromLaunchResponse(launchResponse)
            cachedLaunches.set(launch.id, launch)
            launches.add(launch)
        }
        return launches
    }

    override suspend fun getRocket(id: String): Rocket {
        var rocket = cachedRockets.get(id)
        if (rocket == null) {
            val response = spaceXApiService.getRocket(id)
            if (!response.isSuccessful)
                when (response.code()) {
                    404 -> throw RuntimeException("Rocket $id not found")
                    else -> throw RuntimeException("GetRocket failed with HTTP code ${response.code()}")
                }
            if (response.body() == null)
                throw RuntimeException("SpaceX API returned an invalid value")

            val rocketResponse = response.body()!!
            rocket = Rocket().apply {
                this.id = rocketResponse.id
                name = rocketResponse.name
            }
            cachedRockets.set(id, rocket)
        }
        return rocket
    }

    override suspend fun getLaunch(id: String): Launch {
        var launch = cachedLaunches.get(id)
        if (launch == null) {
            val response = spaceXApiService.getLaunch(id)
            if (!response.isSuccessful)
                when (response.code()) {
                    404 -> throw RuntimeException("Launch $id not found")
                    else -> throw RuntimeException("GetLaunch failed with HTTP code ${response.code()}")
                }
            if (response.body() == null)
                throw RuntimeException("SpaceX API returned an invalid value")

            val launchResponse = response.body()!!
            launch = createLaunchFromLaunchResponse(launchResponse)
            cachedLaunches.set(id, launch)
        }
        return launch
    }

    private suspend fun createLaunchFromLaunchResponse(launchResponse: SpaceXApiService.GetLaunchResponse): Launch {
        return Launch().apply {
            this.id = launchResponse.id
            name = launchResponse.name
            rocket = getRocket(launchResponse.rocket)
            imageUrl = launchResponse.links.flickr.original.let {
                if (it.count() > 0) it[0] else ""
            }
            youtubeVideoId = launchResponse.links.youtubeId ?: ""
            patchUrl = launchResponse.links.patch.small
            date = Instant.ofEpochSecond(launchResponse.dateUnix)
            datePrecision = launchResponse.datePrecision.let {
                when (it) {
                    "half" -> DatePrecision.half
                    "quarter" -> DatePrecision.quarter
                    "year" -> DatePrecision.year
                    "month" -> DatePrecision.month
                    "day" -> DatePrecision.day
                    "hour" -> DatePrecision.hour
                    else -> DatePrecision.year
                }
            }
            flightNumber = launchResponse.flightNumber
            details = launchResponse.details
        }
    }

    /************************************************/
    /************* Caching entities *****************/
    /************************************************/

    private class Cache<T> {

        private val cachedEntities = mutableMapOf<String, CachedEntity<T>>()

        fun get(id: String): T? {
            val cachedEntity = cachedEntities[id]
            return if (cachedEntity != null && cachedEntity.updatedAt + cachedEntity.validPeriod > Instant.now())
                cachedEntity.entity
            else
                null
        }

        fun set(id: String, entity: T, validPeriod: Duration = Duration.ofMinutes(5)) {
            val cachedEntity = cachedEntities[id]?.also {
                it.entity = entity
                it.validPeriod = validPeriod
                it.updatedAt = Instant.now()
            } ?: CachedEntity(entity, validPeriod)
            cachedEntities[id] = cachedEntity
        }

        private class CachedEntity<T>(
            var entity: T,
            var validPeriod: Duration
        ) {
            var updatedAt = Instant.now()!!
        }
    }
}