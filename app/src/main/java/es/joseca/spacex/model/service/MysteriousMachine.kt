package es.joseca.spacex.model.service

import java.time.Instant

interface MysteriousMachine {

    companion object {

        const val maxGaugeValue = 87.0

        const val minGaugeValue = 0.0

        const val maxRatio = 1f

        const val minRatio = 0.1f

        const val maxSignal = 5
    }

    fun setRatio(ratio: Float)

    fun setPressedButton(isPressed: Boolean)

    fun addListener(listener: Listener)

    fun removeListener(listener: Listener)

    interface Listener {

        fun onMagicNumberUpdated(time: Instant, value: Double)

        fun onGaugeUpdated(value: Double)

        fun onSignalUpdated(value: Int)
    }
}