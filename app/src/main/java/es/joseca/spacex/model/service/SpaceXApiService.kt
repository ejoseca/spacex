package es.joseca.spacex.model.service

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface SpaceXApiService {

    @GET("rockets")
    suspend fun getRockets(): Response<List<GetRocketResponse>>

    @GET("rockets/{id}")
    suspend fun getRocket(@Path("id") id: String): Response<GetRocketResponse>

    @POST("launches/query")
    suspend fun getLaunches(@Body query: LaunchQuery): Response<QueryLaunchesResponse>

    @GET("launches/{id}")
    suspend fun getLaunch(@Path("id") id: String): Response<GetLaunchResponse>

    class LaunchQuery {

        @Expose
        @SerializedName("query")
        val query = Query()

        class Query {

            @Expose
            @SerializedName("rocket")
            var rocket: Any? = null
        }

        @Expose
        @SerializedName("options")
        val options = Options()

        class Options {

            @Expose
            @SerializedName("offset")
            var offset: Int = 0

            @Expose
            @SerializedName("limit")
            var limit: Int = 10
        }
    }

    class GetRocketResponse {

        @Expose
        @SerializedName("id")
        var id: String = ""

        @Expose
        @SerializedName("name")
        var name: String = ""
    }

    class GetLaunchResponse {

        @Expose
        @SerializedName("flight_number")
        var flightNumber = 0

        @Expose
        @SerializedName("id")
        var id: String = ""

        @Expose
        @SerializedName("name")
        var name: String = ""

        @Expose
        @SerializedName("date_unix")
        var dateUnix: Long = 0

        @Expose
        @SerializedName("date_precision")
        var datePrecision: String = "year"

        @Expose
        @SerializedName("rocket")
        var rocket: String = ""

        @Expose
        @SerializedName("details")
        var details: String? = null

        @Expose
        @SerializedName("links")
        var links = Links()

        class Links {

            @Expose
            @SerializedName("patch")
            var patch = Patch()

            class Patch {

                @Expose
                @SerializedName("small")
                var small: String? = null
            }

            @Expose
            @SerializedName("flickr")
            var flickr = Flickr()

            class Flickr {

                @Expose
                @SerializedName("original")
                var original: List<String> = listOf()
            }

            @Expose
            @SerializedName("youtube_id")
            var youtubeId: String? = null
        }
    }

    class QueryLaunchesResponse {

        @Expose
        @SerializedName("docs")
        var docs: List<GetLaunchResponse> = listOf()
    }
}