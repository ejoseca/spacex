package es.joseca.spacex.model.service

import com.google.android.youtube.player.YouTubeInitializationResult

interface YoutubeOfficialPlayerService {

    fun setOnInitializeListener(listener: OnInitializedListener)

    fun setVideo(youtubeVideoId: String)

    fun playVideo()

    interface OnInitializedListener {

        fun onInitializationSuccess()

        fun onInitializationFailure(result: YouTubeInitializationResult)
    }
}