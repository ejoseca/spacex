package es.joseca.spacex.model.service

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions

interface YoutubePlayerService {

    fun initialize(
        youTubePlayerListener: YouTubePlayerListener,
        handleNetworkEvents: Boolean
    )
}