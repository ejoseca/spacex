package es.joseca.spacex.model.service.implementation

import android.content.Context
import android.util.Log
import dagger.hilt.android.qualifiers.ApplicationContext
import es.joseca.spacex.model.service.MysteriousMachine
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.pow
import kotlin.random.Random
import kotlin.time.ExperimentalTime
import kotlin.time.milliseconds

@ExperimentalTime
@Singleton
class MysteriousMachineImpl @Inject constructor(@ApplicationContext private val context: Context) :
    MysteriousMachine {

    private var ratio = MysteriousMachine.minRatio

    private var isButtonPressed = false

    private var gaugeValue = MysteriousMachine.minGaugeValue

    private var magicNumber = 0.0

    private var signal = 0

    private val listeners = mutableListOf<MysteriousMachine.Listener>()

    init {
        GlobalScope.launch {
            while (true) {
                delay(100.milliseconds)
                gaugeValue = (gaugeValue + if (isButtonPressed) 1.0 else -1.0)
                    .coerceIn(MysteriousMachine.minGaugeValue, MysteriousMachine.maxGaugeValue)
                magicNumber = (magicNumber + ratio.pow(2) * 4.0).takeIf { it < 9999.0 } ?: 0.0
                val time = Instant.now()
                signal = if (Random.nextFloat() > 0.9) Random.nextInt(-5, 6) else signal
                GlobalScope.launch {
                    for (listener in listeners) {
                        try {
                            listener.onMagicNumberUpdated(time, magicNumber)
                        } catch (ex: Exception) {
                            Log.e("MysteriousMachine", ex.stackTraceToString(), ex)
                        }
                        try {
                            listener.onGaugeUpdated(gaugeValue)
                        } catch (ex: Exception) {
                            Log.e("MysteriousMachine", ex.stackTraceToString(), ex)
                        }
                        try {
                            listener.onSignalUpdated(signal)
                        } catch (ex: Exception) {
                            Log.e("MysteriousMachine", ex.stackTraceToString(), ex)
                        }
                    }
                }
            }
        }
    }

    override fun setRatio(ratio: Float) {
        this.ratio = ratio.coerceIn(MysteriousMachine.minRatio, MysteriousMachine.maxRatio)
    }

    override fun setPressedButton(isPressed: Boolean) {
        isButtonPressed = isPressed
    }

    override fun addListener(listener: MysteriousMachine.Listener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: MysteriousMachine.Listener) {
        listeners.remove(listener)
    }
}