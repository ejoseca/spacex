package es.joseca.spacex.ui

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import es.joseca.spacex.databinding.RocketLaunchVideoActivityBinding
import es.joseca.spacex.model.service.YoutubePlayerService
import javax.inject.Inject

@AndroidEntryPoint
class RocketLaunchVideoActivity : AppCompatActivity() {

    private val args: RocketLaunchVideoActivityArgs by navArgs()

    private val viewModel: RocketLaunchVideoViewModel by viewModels()

    @Inject
    lateinit var youtubePlayerService: YoutubePlayerServiceImpl

    private lateinit var binding: RocketLaunchVideoActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        // Since Android 11.0 (API 30), 'setSystemUiVisibility()' is deprecated, so
        // 'setDecorFitsSystemWindows()' should be used, but I don't have an Android 11 to test it.
        @Suppress("DEPRECATION")
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        super.onCreate(savedInstanceState)

        youtubePlayerService.setActivity(this)
        binding = RocketLaunchVideoActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.lifecycleOwner = this

        viewModel.initialize(args.youtubeVideoId)
        binding.viewModel = viewModel

        lifecycle.addObserver(binding.youtubePlayer)
    }

    override fun onDestroy() {
        youtubePlayerService.unsetActivity(this)
        super.onDestroy()
    }

    @ActivityRetainedScoped
    class YoutubePlayerServiceImpl @Inject constructor() : YoutubePlayerService {

        private var activity: RocketLaunchVideoActivity? = null

        fun setActivity(activity: RocketLaunchVideoActivity) {
            this.activity = activity
        }

        fun unsetActivity(activity: RocketLaunchVideoActivity) {
            if (this.activity == activity) {
                this.activity = null
            }
        }

        override fun initialize(
            youTubePlayerListener: YouTubePlayerListener,
            handleNetworkEvents: Boolean
        ) {
            activity?.binding?.youtubePlayer?.initialize(
                youTubePlayerListener,
                handleNetworkEvents,
                IFramePlayerOptions.Builder()
                    .controls(0)
                    .rel(0)
                    .ivLoadPolicy(3)
                    .ccLoadPolicy(1)
                    .build()
            )
        }
    }

    @Module
    @InstallIn(ActivityRetainedComponent::class)
    abstract class YoutubePlayerHiltModule {

        @Binds
        abstract fun bindYoutubePlayer(impl: YoutubePlayerServiceImpl): YoutubePlayerService
    }
}