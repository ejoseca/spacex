package es.joseca.spacex.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import dagger.hilt.android.lifecycle.HiltViewModel
import es.joseca.spacex.model.service.MysteriousMachine
import es.joseca.spacex.model.service.YoutubePlayerService
import kotlinx.coroutines.launch
import java.time.Duration
import java.time.Instant
import javax.inject.Inject

@HiltViewModel
class RocketLaunchVideoViewModel @Inject constructor(
    private val youtubePlayerService: YoutubePlayerService,
    private val machine: MysteriousMachine
) :
    ViewModel() {

    private lateinit var youtubeVideoId: String

    private var isInitialized = false

    private val _magicNumber = MutableLiveData(0.0)
    val magicNumber: LiveData<Double> = _magicNumber

    private val _gaugePosition = MutableLiveData(MysteriousMachine.minGaugeValue)
    val gaugePosition: LiveData<Double> = _gaugePosition

    private val _magicNumberSpeed = MutableLiveData(0.0)
    val magicNumberSpeed: LiveData<Double> = _magicNumberSpeed

    val ratio = MutableLiveData(0f)

    val isBigButtonPressed = MutableLiveData(false)

    private val _signal = MutableLiveData(0)
    val signal: LiveData<Int> = _signal

    fun initialize(youtubeVideoId: String) {
        if (isInitialized) {
            throw IllegalStateException("RocketLaunchVideoViewModel already initialized")
        }
        this.youtubeVideoId = youtubeVideoId
        youtubePlayerService.initialize(YouTubePlayerListenerImpl(), true)
        machine.addListener(MysteriousMachineListener())
        ratio.observeForever { machine.setRatio(ratio.value!!) }
        isBigButtonPressed.observeForever { machine.setPressedButton(isBigButtonPressed.value!!) }
        isInitialized = true
    }

    private inner class YouTubePlayerListenerImpl : YouTubePlayerListener {
        override fun onApiChange(youTubePlayer: YouTubePlayer) {
        }

        override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
        }

        override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
        }

        override fun onPlaybackQualityChange(
            youTubePlayer: YouTubePlayer,
            playbackQuality: PlayerConstants.PlaybackQuality
        ) {
        }

        override fun onPlaybackRateChange(
            youTubePlayer: YouTubePlayer,
            playbackRate: PlayerConstants.PlaybackRate
        ) {
        }

        override fun onReady(youTubePlayer: YouTubePlayer) {
            youTubePlayer.cueVideo(youtubeVideoId, 0f)
        }

        override fun onStateChange(
            youTubePlayer: YouTubePlayer,
            state: PlayerConstants.PlayerState
        ) {
        }

        override fun onVideoDuration(youTubePlayer: YouTubePlayer, duration: Float) {
        }

        override fun onVideoId(youTubePlayer: YouTubePlayer, videoId: String) {
            youTubePlayer.play()
        }

        override fun onVideoLoadedFraction(youTubePlayer: YouTubePlayer, loadedFraction: Float) {
        }
    }

    private inner class MysteriousMachineListener : MysteriousMachine.Listener {

        private var lastTime: Instant? = null
        private var lastValue: Double? = null

        override fun onMagicNumberUpdated(time: Instant, value: Double) {
            viewModelScope.launch {
                _magicNumber.value = value
                if (lastTime != null) {
                    _magicNumberSpeed.value = (value - lastValue!!) /
                            Duration.between(lastTime!!, time).let { it.seconds + it.nano / 1e9 }
                }
                lastTime = time
                lastValue = value
            }
        }

        override fun onGaugeUpdated(value: Double) {
            viewModelScope.launch {
                _gaugePosition.value = value
            }
        }

        override fun onSignalUpdated(value: Int) {
            viewModelScope.launch {
                _signal.value = value
            }
        }
    }
}