package es.joseca.spacex.ui

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import es.joseca.spacex.R
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.databinding.RocketLaunchesActivityBinding
import javax.inject.Inject

@AndroidEntryPoint
class RocketLaunchesActivity : AppCompatActivity() {

    lateinit var binding: RocketLaunchesActivityBinding

    private val viewModel: RocketLaunchesViewModel by viewModels()

    @Inject
    lateinit var navigator: RocketLaunchesNavigatorImpl

    @Inject
    lateinit var spaceXContext: SpaceXContext

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = if (spaceXContext.rocketLaunchesSettings.landscapeLayout)
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        else
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding = RocketLaunchesActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navigator.setActivity(this)
        setSupportActionBar(binding.toolbar)
    }

    override fun onDestroy() {
        navigator.unsetActivity(this)
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> viewModel.onBackPressed()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onBackPressed() {
        viewModel.onBackPressed()
    }

    @ActivityRetainedScoped
    class RocketLaunchesNavigatorImpl @Inject constructor() :
        RocketLaunchesNavigator {

        private var activity: RocketLaunchesActivity? = null

        fun setActivity(activity: RocketLaunchesActivity) {
            this.activity = activity
        }

        fun unsetActivity(activity: RocketLaunchesActivity) {
            if (this.activity == activity) {
                this.activity = null
            }
        }

        override fun goToDetail() {
            val navController = activity?.binding!!.fragmentContainer?.findNavController()
            if (navController != null) {
                val recyclerView =
                    activity!!.binding.root.findViewById<RecyclerView>(R.id.master_list_recycler_view)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val itemView =
                    layoutManager.findViewByPosition(activity!!.viewModel.selectedMasterListItemIndex.value!!)
                val extras = itemView?.findViewById<ImageView>(R.id.picture)?.let {
                    FragmentNavigatorExtras(it to activity!!.getString(R.string.launch_picture_transition_name))
                }
                recyclerView.isNestedScrollingEnabled = false
                navController.navigate(
                    R.id.to_detail,
                    null,
                    null,
                    extras
                )
            }
        }

        override fun goToVideo(youtubeVideoId: String) {
            val activity = activity
            if (activity != null) {
                val navController = activity.binding.fragmentContainer?.findNavController()
                    ?: activity.binding.masterListFragmentContainer?.findNavController()
                navController?.navigate(
                    if (activity.spaceXContext.rocketLaunchVideoSettings.useOfficialApi)
                        RocketLaunchesDetailFragmentDirections.toOfficialVideo(youtubeVideoId)
                    else
                        RocketLaunchesDetailFragmentDirections.toVideo(youtubeVideoId)
                )
            }
        }

        override fun goUp() {
            val navController = activity?.binding!!.fragmentContainer?.findNavController()
            val wentUp = navController?.navigateUp()
            if (wentUp != true)
                activity?.finish()
        }
    }

    @Module
    @InstallIn(ActivityRetainedComponent::class)
    abstract class NavigatorHiltModule {

        @Binds
        abstract fun bindNavigator(impl: RocketLaunchesNavigatorImpl): RocketLaunchesNavigator
    }
}