package es.joseca.spacex.ui

import android.animation.AnimatorInflater
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.addListener
import androidx.fragment.app.activityViewModels
import androidx.transition.Transition
import androidx.transition.TransitionInflater
import androidx.transition.TransitionListenerAdapter
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import dagger.hilt.android.AndroidEntryPoint
import es.joseca.spacex.R
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.databinding.RocketLaunchesDetailFragmentBinding
import javax.inject.Inject

@AndroidEntryPoint
class RocketLaunchesDetailFragment : Fragment(), RequestListener<Drawable> {

    private lateinit var binding: RocketLaunchesDetailFragmentBinding

    private val viewModel: RocketLaunchesViewModel by activityViewModels()

    @Inject
    lateinit var spaceXContext: SpaceXContext

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.detailedItem.value?.backgroundImageUrl?.value?.isNotEmpty() == true) {
            val transition = TransitionInflater.from(requireContext())
                .inflateTransition(R.transition.to_detail_image_transition)
            sharedElementEnterTransition = transition
            sharedElementReturnTransition = TransitionInflater.from(requireContext())
                .inflateTransition(R.transition.to_detail_image_transition)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RocketLaunchesDetailFragmentBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (spaceXContext.rocketLaunchesSettings.navigateToDetail &&
            viewModel.detailedItem.value?.backgroundImageUrl?.value?.isNotEmpty() == true
        ) {
            postponeEnterTransition()
            val transition = sharedElementEnterTransition as Transition
            transition.addListener(object : TransitionListenerAdapter() {
                override fun onTransitionEnd(transition: Transition) {
                    runEnterAnimations()
                }
            })
        } else {
            binding.root.post {
                runEnterAnimations()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (spaceXContext.rocketLaunchesSettings.navigateToDetail) {
            (activity as RocketLaunchesActivity).binding.appBarLayout.setExpanded(false, true)
        }
    }

    override fun onResume() {
        super.onResume()
        binding.root.post {
            if (!binding.dataContainer.hasEntered) {
                runEnterAnimations()
            }
        }
    }

    private fun runEnterAnimations() {
        val containerAnimator = binding.dataContainer.enterWithAnimation()
        containerAnimator.addListener(onEnd = {
            AnimatorInflater.loadAnimator(context, R.animator.enter_rotating_and_scaling).apply {
                setTarget(binding.playVideo)
                start()
            }
        })
    }

    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        Handler(Looper.getMainLooper()).post {
            startPostponedEnterTransition()
        }
        return false
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        Handler(Looper.getMainLooper()).post {
            startPostponedEnterTransition()
        }
        return false
    }
}