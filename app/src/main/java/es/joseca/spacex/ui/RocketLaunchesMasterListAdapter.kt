package es.joseca.spacex.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import es.joseca.spacex.R
import es.joseca.spacex.common.ListChangesToRecyclerListener
import es.joseca.spacex.databinding.RocketLaunchesMasterListItemViewBinding
import es.joseca.spacex.databinding.RocketLaunchesMasterListItemWithPictureViewBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class RocketLaunchesMasterListAdapter(private val viewModel: RocketLaunchesViewModel) :
    RecyclerView.Adapter<RocketLaunchesMasterListAdapter.RocketLaunchesMasterListItemViewHolder>() {

    /**
     * This object listens to ViewModel's list changes and notifies them to the RecyclerView
     */
    private val masterListChangedListener =
        ListChangesToRecyclerListener<RocketLaunchesViewModel.MasterListLaunchViewModel>(this)

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        viewModel.masterList.addOnListChangedCallback(masterListChangedListener)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        viewModel.masterList.removeOnListChangedCallback(masterListChangedListener)
    }

    @ExperimentalCoroutinesApi
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RocketLaunchesMasterListItemViewHolder {
        val binding = if (viewType == ViewType.WithImage.value)
            RocketLaunchesMasterListItemWithPictureViewBinding.bind(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.rocket_launches_master_list_item_with_picture_view,
                    parent,
                    false
                )
            )
        else
            RocketLaunchesMasterListItemViewBinding.bind(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.rocket_launches_master_list_item_view, parent, false
                )
            )
        return RocketLaunchesMasterListItemViewHolder(binding)
    }

    @ExperimentalCoroutinesApi
    override fun onBindViewHolder(holder: RocketLaunchesMasterListItemViewHolder, position: Int) {
        holder.itemViewModel = viewModel.masterList[position]
    }

    override fun getItemCount(): Int = viewModel.masterList.count()

    override fun getItemViewType(position: Int): Int {
        return if (viewModel.masterList[position].pictureUrl.value?.isNotEmpty() == true)
            ViewType.WithImage.value
        else
            ViewType.WithoutImage.value
    }


    /*********************************/
    /*********** Classes *************/
    /*********************************/

    @ExperimentalCoroutinesApi
    class RocketLaunchesMasterListItemViewHolder(private val binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION)
                    itemViewModel?.tapOnMasterListItem(adapterPosition)
            }
        }

        var itemViewModel: RocketLaunchesViewModel.MasterListLaunchViewModel? = null
            set(value) {
                field = value
                if (binding is RocketLaunchesMasterListItemViewBinding)
                    binding.viewModel = value
                else if (binding is RocketLaunchesMasterListItemWithPictureViewBinding)
                    binding.viewModel = value
            }
    }

    enum class ViewType(val value: Int) {
        WithImage(1),
        WithoutImage(2);
    }
}