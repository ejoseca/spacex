package es.joseca.spacex.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import es.joseca.spacex.R
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.common.ui.BiggerLinearLayoutManager
import es.joseca.spacex.databinding.RocketLaunchesMasterListFragmentBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RocketLaunchesMasterListFragment : Fragment() {

    private lateinit var binding: RocketLaunchesMasterListFragmentBinding

    private val viewModel: RocketLaunchesViewModel by activityViewModels()

    @Inject
    lateinit var spaceXContext: SpaceXContext

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RocketLaunchesMasterListFragmentBinding.bind(
            inflater.inflate(
                R.layout.rocket_launches_master_list_fragment,
                container,
                false
            )
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // SwipeRefresh
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.viewModelScope.launch {
                viewModel.onRefreshMasterList()
            }
        }
        viewModel.isMasterDetailLoadingIconVisible.observe(viewLifecycleOwner, {
            binding.swipeRefresh.isRefreshing = it
        })
        binding.swipeRefresh.isRefreshing = viewModel.isMasterDetailLoadingIconVisible.value!!

        // Spinner
        binding.rocketSpinner.adapter = RocketLaunchesMasterListRocketsAdapter(viewModel)
        binding.rocketSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.selectedRocket.value = viewModel.rocketList[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.selectedRocket.value = null
            }
        }
        viewModel.isRocketSpinnerEnabled.observe(viewLifecycleOwner, {
            binding.rocketSpinner.isEnabled = it
        })

        // RecyclerView
        val recyclerViewAdapter = RocketLaunchesMasterListAdapter(viewModel)
        binding.masterListRecyclerView.adapter = recyclerViewAdapter
        val recyclerViewLayoutManager = BiggerLinearLayoutManager(requireContext())
        binding.masterListRecyclerView.layoutManager = recyclerViewLayoutManager
        binding.masterListRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val lastVisibleItemPosition =
                    recyclerViewLayoutManager.findLastCompletelyVisibleItemPosition()
                if (lastVisibleItemPosition >= recyclerViewAdapter.itemCount - 1)
                    viewModel.viewModelScope.launch {
                        viewModel.onReachMasterListBottom()
                    }
                viewModel.viewModelScope.launch {
                    viewModel.onScrollMasterList(lastVisibleItemPosition)
                }
            }
        })
        postponeEnterTransition()
        (view.parent as ViewGroup).doOnPreDraw {
            startPostponedEnterTransition()
        }
        if (spaceXContext.rocketLaunchesSettings.navigateToDetail)
            binding.masterListRecyclerView.isNestedScrollingEnabled = true
    }
}