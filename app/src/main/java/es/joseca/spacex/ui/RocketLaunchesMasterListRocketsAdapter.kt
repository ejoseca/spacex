package es.joseca.spacex.ui

import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.databinding.ObservableList
import es.joseca.spacex.R


class RocketLaunchesMasterListRocketsAdapter(private val viewModel: RocketLaunchesViewModel) :
    SpinnerAdapter {

    private val listeners = mutableListOf<RocketListListener>()

    override fun registerDataSetObserver(observer: DataSetObserver) {
        val listener = RocketListListener(observer)
        viewModel.rocketList.addOnListChangedCallback(listener)
        listeners.add(listener)
    }

    override fun unregisterDataSetObserver(observer: DataSetObserver) {
        listeners.find { it.observer == observer }?.apply {
            viewModel.rocketList.removeOnListChangedCallback(this)
            listeners.remove(this)
        }
    }

    override fun getCount(): Int {
        return viewModel.rocketList.size
    }

    override fun getItem(position: Int): Any {
        return viewModel.rocketList[position]
    }

    override fun getItemId(position: Int): Long {
        return if (position >= 0 && position < viewModel.rocketList.size)
            viewModel.rocketList[position].hashCode().toLong()
        else
            0
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(R.layout.rocket_launches_master_list_rocket_view, parent, false)
        val textView = view.findViewById<TextView>(R.id.text_view)
        textView.text = viewModel.rocketList[position].name.value
        return view
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun isEmpty(): Boolean {
        return viewModel.rocketList.isEmpty()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(R.layout.rocket_launches_master_list_rocket_dropdown_view, parent, false)
        val textView = view.findViewById<TextView>(R.id.text_view)
        textView.text = viewModel.rocketList[position].name.value
        return view
    }

    private class RocketListListener(val observer: DataSetObserver) :
        ObservableList.OnListChangedCallback<ObservableList<RocketLaunchesViewModel.RocketViewModel>>() {

        override fun onChanged(sender: ObservableList<RocketLaunchesViewModel.RocketViewModel>?) {
            observer.onChanged()
        }

        override fun onItemRangeChanged(
            sender: ObservableList<RocketLaunchesViewModel.RocketViewModel>?,
            positionStart: Int,
            itemCount: Int
        ) {
        }

        override fun onItemRangeInserted(
            sender: ObservableList<RocketLaunchesViewModel.RocketViewModel>?,
            positionStart: Int,
            itemCount: Int
        ) {
        }

        override fun onItemRangeMoved(
            sender: ObservableList<RocketLaunchesViewModel.RocketViewModel>?,
            fromPosition: Int,
            toPosition: Int,
            itemCount: Int
        ) {
        }

        override fun onItemRangeRemoved(
            sender: ObservableList<RocketLaunchesViewModel.RocketViewModel>?,
            positionStart: Int,
            itemCount: Int
        ) {
        }
    }
}