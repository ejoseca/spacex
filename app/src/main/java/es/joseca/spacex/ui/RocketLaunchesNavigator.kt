package es.joseca.spacex.ui

interface RocketLaunchesNavigator {

    fun goToDetail()

    fun goUp()

    fun goToVideo(youtubeVideoId: String)
}