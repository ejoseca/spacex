package es.joseca.spacex.ui

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.common.ui.MasterDetailViewModel
import es.joseca.spacex.model.entity.DatePrecision
import es.joseca.spacex.model.entity.Launch
import es.joseca.spacex.model.entity.Rocket
import es.joseca.spacex.model.repository.SpaceXApiRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RocketLaunchesViewModel @Inject constructor(
    private val context: SpaceXContext,
    private val navigator: RocketLaunchesNavigator,
    private val apiRepository: SpaceXApiRepository
) : MasterDetailViewModel<RocketLaunchesViewModel.MasterListLaunchViewModel, RocketLaunchesViewModel.DetailLaunchViewModel>() {

    private val _isMasterDetailLoadingIconVisible = MutableLiveData(false)
    val isMasterDetailLoadingIconVisible: LiveData<Boolean> = _isMasterDetailLoadingIconVisible

    private val _rocketList =
        ObservableArrayList<RocketViewModel>().apply { add(RocketViewModel.ALL) }
    val rocketList: ObservableList<RocketViewModel> = _rocketList

    val selectedRocket = MutableLiveData<RocketViewModel>()

    private val _isRocketSpinnerEnabled = MutableLiveData(true)
    val isRocketSpinnerEnabled: LiveData<Boolean> = _isRocketSpinnerEnabled

    private var isRefreshing = false

    private var isLoadingMore = false

    private var isLoadingRockets = false

    private var isGoingToVideo = false

    private var currentListRocket: Rocket? = RocketViewModel.ALL.rocket

    /**
     * True when there aren't more items to load
     */
    private var masterListEndReached = false

    private var isInDetailByNavigation = false

    init {
        viewModelScope.launch {
            reloadMasterList()
        }
        viewModelScope.launch {
            while (true) {
                try {
                    loadRockets()
                    break
                } catch (ex: Exception) {
                    Log.e(RocketLaunchesViewModel::class.simpleName, ex.message, ex)
                    delay(1000)
                }
            }
        }
        selectedRocket.observeForever {
            val rocketId = selectedRocket.value?.rocket?.id
            if (rocketId?.isNotEmpty() == true && rocketId != currentListRocket?.id ||
                rocketId?.isEmpty() != false && currentListRocket?.id?.isNotEmpty() == true
            )
                viewModelScope.launch { reloadMasterList() }
        }
    }


    /******************************************/
    /*********** User interactions ************/
    /******************************************/

    @ExperimentalCoroutinesApi
    override fun onTapOnMasterListItem(index: Int) {
        if (isGoingToVideo) {
            return
        }
        if (isInDetailByNavigation) {
            // Avoid click in more than one item at the same time
            return
        }
        super.onTapOnMasterListItem(index)
        if (context.rocketLaunchesSettings.navigateToDetail) {
            navigator.goToDetail()
            isInDetailByNavigation = true
        }
    }

    override suspend fun onFilterMasterListByQuery(query: String) {
        // Don't filter by query
    }

    override suspend fun onReachMasterListBottom() {
        loadMoreMasterListItems()
    }

    override suspend fun onRefreshMasterList() {
        reloadMasterList()
    }

    suspend fun onScrollMasterList(lastVisibleItemPosition: Int) {
        if (masterList.count() - lastVisibleItemPosition < context.rocketLaunchesSettings.launchesPerPage)
            loadMoreMasterListItems()
    }

    fun onTapDetailVideoButton() {
        if (isGoingToVideo) {
            return
        }
        val youtubeVideoId = detailedItem.value?.youtubeVideoId
        if (youtubeVideoId?.isEmpty() != false) {
            return
        }
        // isGoingToVideo does nothing here, but I plan to fix it
        isGoingToVideo = true
        try {
            navigator.goToVideo(youtubeVideoId)
        } finally {
            isGoingToVideo = false
        }
    }

    fun onBackPressed() {
        navigator.goUp()
        if (isInDetailByNavigation) {
            isInDetailByNavigation = false
        }
    }


    /**********************************************/
    /************ Internal functions **************/
    /**********************************************/

    private suspend fun loadRockets() {
        if (isLoadingRockets) {
            return
        }
        isLoadingRockets = true
        try {
            val rockets = apiRepository.getRockets().map { RocketViewModel(it) }
            _rocketList.clear()
            _rocketList.add(RocketViewModel.ALL)
            _rocketList.addAll(rockets)
        } finally {
            isLoadingRockets = false
        }
    }

    private suspend fun reloadMasterList() {
        if (isRefreshing || isLoadingMore) {
            return
        }
        isRefreshing = true
        _isMasterDetailLoadingIconVisible.value = true
        _isRocketSpinnerEnabled.value = false
        try {
            val limit = context.rocketLaunchesSettings.launchesPerPage
            val rocket = selectedRocket.value?.rocket
            val newItems = (
                    if (rocket?.id?.isNotEmpty() == true)
                        apiRepository.getPaginatedLaunches(rocket, 0, limit)
                    else
                        apiRepository.getPaginatedLaunches(0, limit)
                    ).map { MasterListLaunchViewModel(it) }
            internalMasterList.clear()
            internalMasterList.addAll(newItems)
            currentListRocket = rocket
            masterListEndReached = newItems.count() < limit
        } finally {
            isRefreshing = false
            _isMasterDetailLoadingIconVisible.value = false
            _isRocketSpinnerEnabled.value = true
        }
    }

    private suspend fun loadMoreMasterListItems() {
        if (isLoadingMore || isRefreshing) {
            return
        }
        if (masterListEndReached) {
            return
        }
        isLoadingMore = true
        _isMasterDetailLoadingIconVisible.value = true
        _isRocketSpinnerEnabled.value = false
        try {
            val limit = context.rocketLaunchesSettings.launchesPerPage
            val rocket = selectedRocket.value?.rocket
            val newItems = (
                    if (rocket?.id?.isNotEmpty() == true)
                        apiRepository.getPaginatedLaunches(rocket, masterList.count(), limit)
                    else
                        apiRepository.getPaginatedLaunches(masterList.count(), limit)
                    ).map { MasterListLaunchViewModel(it) }
            // Cannot remove items because of the observable notifications
            //internalMasterList.removeAll(newItems)
            internalMasterList.addAll(newItems)
            if (newItems.count() < limit) {
                masterListEndReached = true
            }
        } finally {
            isLoadingMore = false
            _isMasterDetailLoadingIconVisible.value = false
            _isRocketSpinnerEnabled.value = true
        }
    }

    override suspend fun getDetailedItem(masterListItem: MasterListLaunchViewModel): DetailLaunchViewModel? {
        return apiRepository.getLaunch(masterListItem.id.value!!)?.let { DetailLaunchViewModel(it) }
    }


    /**********************************************/
    /*************** View Models ******************/
    /**********************************************/

    class RocketViewModel(val rocket: Rocket) {

        companion object {

            // This is wrong. Text must be stored in resources.
            val ALL = RocketViewModel(Rocket().apply { name = "All rockets" })
        }

        private val _id = MutableLiveData(rocket.id)
        val id: LiveData<String> = _id

        private val _name = MutableLiveData(rocket.name)
        val name: LiveData<String> = _name
    }

    inner class MasterListLaunchViewModel(launch: Launch) {

        private val _id = MutableLiveData(launch.id)
        val id: LiveData<String> = _id

        private val _launchName = MutableLiveData(launch.name)
        val launchName: LiveData<String> = _launchName

        private val _rocketName = MutableLiveData(launch.rocket.name)
        val rocketName: LiveData<String> = _rocketName

        private val _launchPatchUrl = MutableLiveData(launch.patchUrl)
        val launchPatchUrl: LiveData<String?> = _launchPatchUrl

        private val _launchDate = MutableLiveData(LaunchDate(launch))
        val launchDate: LiveData<LaunchDate> = _launchDate

        private val _pictureUrl = MutableLiveData(launch.imageUrl)
        val pictureUrl: LiveData<String> = _pictureUrl

        inner class LaunchDate(launch: Launch) {

            private val utcDateTime = LocalDateTime.ofInstant(launch.date, ZoneOffset.UTC)

            val year = utcDateTime.year
        }

        override fun equals(other: Any?): Boolean {
            if (other is MasterListLaunchViewModel) {
                return other.id.value == id.value
            }
            return super.equals(other)
        }

        override fun hashCode(): Int {
            return id.value.hashCode()
        }

        @ExperimentalCoroutinesApi
        fun tapOnMasterListItem(index: Int) {
            this@RocketLaunchesViewModel.onTapOnMasterListItem(index)
        }
    }

    inner class DetailLaunchViewModel(launch: Launch) {

        var youtubeVideoId = launch.youtubeVideoId
            private set(value) {
                field = value
                _showVideoButton.value = value.isNotEmpty()
            }

        private val _id = MutableLiveData(launch.id)
        val id: LiveData<String> = _id

        private val _launchName = MutableLiveData(launch.name)
        val launchName: LiveData<String> = _launchName

        private val _rocketName = MutableLiveData(launch.rocket.name)
        val rocketName: LiveData<String> = _rocketName

        private val _backgroundImageUrl = MutableLiveData(launch.imageUrl)
        val backgroundImageUrl: LiveData<String> = _backgroundImageUrl

        private val _showVideoButton = MutableLiveData(launch.youtubeVideoId.isNotEmpty())
        val showVideoButton: LiveData<Boolean> = _showVideoButton

        private val _launchDate = MutableLiveData(LaunchDate(launch))
        val launchDate: LiveData<LaunchDate> = _launchDate

        private val _flightNumber = MutableLiveData(launch.flightNumber)
        val flightNumber: LiveData<Int> = _flightNumber

        private val _details = MutableLiveData(launch.details)
        val details: LiveData<String?> = _details

        inner class LaunchDate(launch: Launch) {

            private val utcDateTime = LocalDateTime.ofInstant(launch.date, ZoneOffset.UTC)

            val year = utcDateTime.year

            val month =
                if (launch.datePrecision >= DatePrecision.month) utcDateTime.month.value else null

            val day =
                if (launch.datePrecision >= DatePrecision.day) utcDateTime.dayOfMonth else null
        }

        fun onTapVideoButton() {
            onTapDetailVideoButton()
        }
    }


    /**********************************************/
    /***************** Testing ********************/
    /**********************************************/

    override val test = Test()

    inner class Test :
        MasterDetailViewModel<RocketLaunchesViewModel.MasterListLaunchViewModel, RocketLaunchesViewModel.DetailLaunchViewModel>.Test() {

        fun setStateInDetailView() {
            isInDetailByNavigation = true
        }

        fun setIsRefreshing(isRefreshing: Boolean) {
            this@RocketLaunchesViewModel.isRefreshing = isRefreshing
        }

        fun setIsLoadingMore(isLoadingMore: Boolean) {
            this@RocketLaunchesViewModel.isLoadingMore = isLoadingMore
        }
    }
}