package es.joseca.spacex.ui.discarded

import android.os.Bundle
import android.view.WindowManager
import androidx.navigation.navArgs
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.databinding.RocketLaunchVideoOfficialActivityBinding
import es.joseca.spacex.model.service.YoutubeOfficialPlayerService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RocketLaunchVideoOfficialActivity : YouTubeBaseActivity() {

    private val args: RocketLaunchVideoOfficialActivityArgs by navArgs()

    private lateinit var spaceXContext: SpaceXContext

    private lateinit var binding: RocketLaunchVideoOfficialActivityBinding

    private lateinit var viewModel: RocketLaunchVideoOfficialViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        super.onCreate(savedInstanceState)

        val hiltEntryPoint =
            EntryPointAccessors.fromApplication(applicationContext, HiltEntryPoint::class.java)
        spaceXContext = hiltEntryPoint.spaceXContext()
        val youtubePlayerService = YoutubePlayerServiceImpl()

        viewModel = RocketLaunchVideoOfficialViewModel(youtubePlayerService).apply {
            initialize(args.youtubeVideoId)
        }

        binding = RocketLaunchVideoOfficialActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewModel = viewModel
        binding.youtubePlayer.initialize(
            spaceXContext.rocketLaunchVideoSettings.youtubeApiKey,
            youtubePlayerService
        )
    }


    /***********************************************/
    /********** YoutubePlayerServiceImpl ***********/
    /***********************************************/

    private inner class YoutubePlayerServiceImpl : YoutubeOfficialPlayerService,
        YouTubePlayer.OnInitializedListener,
        YouTubePlayer.PlayerStateChangeListener,
        YouTubePlayer.PlaybackEventListener {

        private var isInitialized = false

        private var player: YouTubePlayer? = null


        /********************************************/
        /********** YoutubePlayerService ************/

        private var onInitializedListener: YoutubeOfficialPlayerService.OnInitializedListener? = null

        override fun setOnInitializeListener(listener: YoutubeOfficialPlayerService.OnInitializedListener) {
            onInitializedListener = listener
            if (isInitialized) GlobalScope.launch {
                listener.onInitializationSuccess()
            }
        }

        override fun setVideo(youtubeVideoId: String) {
            if (!isInitialized) {
                throw IllegalStateException()
            }
            player?.cueVideo(youtubeVideoId)
        }

        override fun playVideo() {
            if (!isInitialized) {
                throw IllegalStateException()
            }
            player?.play()
        }


        /********************************************/
        /********** OnInitializedListener ***********/

        override fun onInitializationSuccess(
            provider: YouTubePlayer.Provider?,
            player: YouTubePlayer,
            wasRestored: Boolean
        ) {
            this.player = player
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL)
            player.setPlaybackEventListener(this)
            player.setPlayerStateChangeListener(this)
            isInitialized = true
            onInitializedListener?.onInitializationSuccess()
        }

        override fun onInitializationFailure(
            provider: YouTubePlayer.Provider?,
            result: YouTubeInitializationResult
        ) {
            isInitialized = false
            onInitializedListener?.onInitializationFailure(result)
        }


        /********************************************/
        /******** PlayerStateChangeListener *********/

        override fun onLoading() {
        }

        override fun onLoaded(p0: String?) {
        }

        override fun onAdStarted() {
        }

        override fun onVideoStarted() {
        }

        override fun onVideoEnded() {
        }

        override fun onError(error: YouTubePlayer.ErrorReason) {
        }


        /********************************************/
        /********** PlaybackEventListener ***********/

        override fun onPlaying() {
        }

        override fun onPaused() {
        }

        override fun onStopped() {
        }

        override fun onBuffering(p0: Boolean) {
        }

        override fun onSeekTo(p0: Int) {
        }
    }


    /***************************/
    /********** Hilt ***********/
    /***************************/

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface HiltEntryPoint {

        fun spaceXContext(): SpaceXContext
    }
}