package es.joseca.spacex.ui.discarded

import androidx.databinding.BaseObservable
import com.google.android.youtube.player.YouTubeInitializationResult
import es.joseca.spacex.model.service.YoutubeOfficialPlayerService

class RocketLaunchVideoOfficialViewModel(private val youtubePlayer: YoutubeOfficialPlayerService) :
    BaseObservable() {

    private var isInitialized = false

    private lateinit var youtubeVideoId: String

    init {
        youtubePlayer.setOnInitializeListener(YoutubeInitializationListener())
    }

    fun initialize(youtubeVideoId: String) {
        this.youtubeVideoId = youtubeVideoId
        isInitialized = true
    }

    private inner class YoutubeInitializationListener : YoutubeOfficialPlayerService.OnInitializedListener {
        override fun onInitializationSuccess() {
            youtubePlayer.setVideo(youtubeVideoId)
            youtubePlayer.playVideo()
        }

        override fun onInitializationFailure(result: YouTubeInitializationResult) {
            TODO("Not yet implemented")
        }
    }
}