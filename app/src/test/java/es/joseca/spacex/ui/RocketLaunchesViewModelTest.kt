package es.joseca.spacex.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import es.joseca.spacex.SpaceXContext
import es.joseca.spacex.TestCoroutineRule
import es.joseca.spacex.model.entity.Launch
import es.joseca.spacex.model.entity.Rocket
import es.joseca.spacex.model.repository.SpaceXApiRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*
import org.mockito.kotlin.any


@RunWith(MockitoJUnitRunner::class)
class RocketLaunchesViewModelTest {

    @Rule
    @JvmField
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Rule
    @JvmField
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var spaceXContext: SpaceXContext

    @Mock
    private lateinit var navigator: RocketLaunchesNavigator

    @Mock
    private lateinit var apiRepository: SpaceXApiRepository

    private lateinit var viewModel: RocketLaunchesViewModel

    @Before
    fun prepareProperties() {
        spaceXContext = SpaceXContext().apply {
            rocketLaunchesSettings.launchesPerPage = 1
        }
        runBlocking {
            apiRepository = mock {
                on(it.getRockets()) doReturn listOf()
                on(it.getPaginatedLaunches(anyInt(), anyInt())) doReturn listOf()
                on(it.getPaginatedLaunches(eq(0), anyInt())) doReturn listOf(Launch().apply {
                    id = "launch123"
                    name = "my launch"
                    youtubeVideoId = "youtube123"
                    rocket = Rocket().apply {
                        id = "rocket123"
                        name = "my rocket"
                    }
                })
                on(it.getPaginatedLaunches(any(), anyInt(), anyInt())) doReturn listOf()
            }
        }
        viewModel = RocketLaunchesViewModel(spaceXContext, navigator, apiRepository)
        clearInvocations(apiRepository)
        clearInvocations(navigator)
    }


    /*************************************************/
    /************* onTapOnMasterListItem *************/
    /*************************************************/

    @ExperimentalCoroutinesApi
    @Test
    fun `must navigate to Launch detail when navigation is enabled`() {
        spaceXContext.rocketLaunchesSettings.navigateToDetail = true
        viewModel.onTapOnMasterListItem(0)
        verify(navigator).goToDetail()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `must navigate to Launch detail ONLY ONCE (avoids double click)`() {
        spaceXContext.rocketLaunchesSettings.navigateToDetail = true
        viewModel.onTapOnMasterListItem(0)
        viewModel.onTapOnMasterListItem(0)
        verify(navigator, atMost(1)).goToDetail()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't navigate to Launch detail when already in Detail view`() {
        viewModel.test.setStateInDetailView()
        viewModel.onTapOnMasterListItem(0)
        verify(navigator, never()).goToDetail()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't navigate to Launch detail when navigation is disabled`() {
        spaceXContext.rocketLaunchesSettings.navigateToDetail = false
        viewModel.onTapOnMasterListItem(0)
        verify(navigator, never()).goToDetail()
    }


    /*************************************************/
    /************ onReachMasterListBottom ************/
    /*************************************************/

    @ExperimentalCoroutinesApi
    @Test
    fun `must load more Launches when the list's bottom is reached`() {
        runBlockingTest {
            viewModel.onReachMasterListBottom()
        }
        verifyBlocking(apiRepository) { getPaginatedLaunches(anyInt(), anyInt()) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't load more Launches when last load retrieved less items than specified`() {
        runBlockingTest {
            viewModel.onReachMasterListBottom()
            viewModel.onReachMasterListBottom()
        }
        verifyBlocking(apiRepository, atMost(1)) { getPaginatedLaunches(anyInt(), anyInt()) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't load more Launches when it is refreshing the list`() {
        viewModel.test.setIsRefreshing(true)
        runBlockingTest {
            viewModel.onReachMasterListBottom()
        }
        verifyBlocking(apiRepository, never()) { getPaginatedLaunches(anyInt(), anyInt()) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't load more Launches when it is already loading more items`() {
        viewModel.test.setIsLoadingMore(true)
        runBlockingTest {
            viewModel.onReachMasterListBottom()
        }
        verifyBlocking(apiRepository, never()) { getPaginatedLaunches(anyInt(), anyInt()) }
    }


    /*************************************************/
    /************* onTapDetailVideoButton ************/
    /*************************************************/

    @ExperimentalCoroutinesApi
    @Test
    fun `must navigate to the video when the 'Play video' button is pressed`() {
        val detailItem = viewModel.DetailLaunchViewModel(Launch().apply {
            youtubeVideoId = "youtube123"
        })
        viewModel.test.setDetailedItem(detailItem)
        viewModel.onTapDetailVideoButton()
        verify(navigator).goToVideo(anyString())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't navigate to the video if there is no detailed launch`() {
        viewModel.onTapDetailVideoButton()
        verify(navigator, never()).goToVideo(anyString())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `mustn't navigate to the video if the detailed Launch doesn't have video`() {
        val detailItem = viewModel.DetailLaunchViewModel(Launch())
        viewModel.test.setDetailedItem(detailItem)
        viewModel.onTapDetailVideoButton()
        verify(navigator, never()).goToVideo(anyString())
    }
}