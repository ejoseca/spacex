package es.joseca.spacex.common

import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView

class ListChangesToRecyclerListener<T>(private val recyclerViewAdapter: RecyclerView.Adapter<*>) : ObservableList.OnListChangedCallback<ObservableList<T>>(){

    override fun onChanged(sender: ObservableList<T>?) {
        // Do nothing
    }

    override fun onItemRangeChanged(
        sender: ObservableList<T>?,
        positionStart: Int,
        itemCount: Int
    ) {
        recyclerViewAdapter.notifyItemRangeChanged(positionStart, itemCount)
    }

    override fun onItemRangeInserted(
        sender: ObservableList<T>?,
        positionStart: Int,
        itemCount: Int
    ) {
        recyclerViewAdapter.notifyItemRangeInserted(positionStart, itemCount)
    }

    override fun onItemRangeMoved(
        sender: ObservableList<T>?,
        fromPosition: Int,
        toPosition: Int,
        itemCount: Int
    ) {
        recyclerViewAdapter.notifyItemRangeRemoved(fromPosition, itemCount)
        recyclerViewAdapter.notifyItemRangeInserted(toPosition, itemCount)
    }

    override fun onItemRangeRemoved(
        sender: ObservableList<T>?,
        positionStart: Int,
        itemCount: Int
    ) {
        recyclerViewAdapter.notifyItemRangeRemoved(positionStart, itemCount)
    }
}