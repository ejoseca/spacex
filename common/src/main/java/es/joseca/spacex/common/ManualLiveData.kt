package es.joseca.spacex.common

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData

/**
 * LiveData instance that can delay the value change notification until it is required.
 * This allows to change several LiveData values and notify them only after all changes have been made.
 */
class ManualLiveData<T> : MutableLiveData<T> {

    /**
     * In order to work with unit tests, we cannot use the Android's main looper.
     */
    private val mainThread = Thread.currentThread()

    private var quietValue: T? = null

    constructor() : super()

    constructor(value: T) : super(value)

    override fun getValue(): T? {
        return quietValue ?: super.getValue()
    }

    override fun setValue(value: T) {
        quietValue = null
        super.setValue(value)
    }

    /**
     * Sets the value without notifying to observers.
     * Be sure to call [notifyQuietValue] after calling this method.
     */
    @MainThread
    fun setValueQuietly(value: T) {
        assertMainThread("setValueQuietly")
        quietValue = value
    }

    /**
     * Notifies the pending value change to observers.
     * This must be called after [setValueQuietly] as soon as possible.
     */
    @MainThread
    fun notifyQuietValue() {
        assertMainThread("notifyQuietValue")
        if (quietValue != null) {
            setValue(quietValue!!)
        }
    }

    private fun assertMainThread(methodName: String) {
        if (Thread.currentThread() != mainThread) {
            throw IllegalStateException("Cannot invoke $methodName on a background thread")
        }
    }
}