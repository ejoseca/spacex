package es.joseca.spacex.common.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View

class BaselineGuide : View {

    constructor (context: Context) : this(context, null)

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    constructor (
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        visibility = INVISIBLE
    }

    override fun getBaseline(): Int {
        return 0
    }
}