package es.joseca.spacex.common.ui

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.joseca.spacex.common.ManualLiveData
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

// This base class was a bad decision. Now it is just a stone in the shoe.
abstract class MasterDetailViewModel<MasterItemType, DetailItemType> : ViewModel() {

    private val _query = MutableLiveData("")
    val query: LiveData<String> = _query

    private val _selectedMasterListItem = ManualLiveData<MasterItemType?>(null)
    val selectedMasterListItem: LiveData<MasterItemType?> = _selectedMasterListItem

    private val _selectedMasterListItemIndex = ManualLiveData<Int?>(null)
    val selectedMasterListItemIndex: LiveData<Int?> = _selectedMasterListItemIndex

    protected val _detailedItem = MutableLiveData<DetailItemType?>(null)
    val detailedItem: LiveData<DetailItemType?> = _detailedItem

    private val _isDetailLoading = MutableLiveData(false)
    val isDetailLoading: LiveData<Boolean> = _isDetailLoading

    protected val internalMasterList = ObservableArrayList<MasterItemType>()
    val masterList: ObservableList<MasterItemType> = internalMasterList


    /*******************************************************************/
    /************* Public methods called from the view *****************/
    /*******************************************************************/

    fun setQuery(query: String) {
        _query.value = query
        viewModelScope.launch {
            onFilterMasterListByQuery(query)
        }
    }

    @ExperimentalCoroutinesApi
    open fun onTapOnMasterListItem(index: Int) {
        viewModelScope.launch(start = CoroutineStart.UNDISPATCHED) {
            selectItemFromMasterList(index)
        }
    }

    /**
     * When using pagination, adds more items to the master list
     */
    abstract suspend fun onReachMasterListBottom()

    /**
     * Reloads the list of items from the beginning
     */
    abstract suspend fun onRefreshMasterList()


    /*******************************************************************/
    /************ Methods related to individual list items *************/
    /*******************************************************************/

    private suspend fun selectItemFromMasterList(index: Int) {
        if (index < 0 || index >= masterList.count()) {
            return
        }
        _selectedMasterListItemIndex.setValueQuietly(index)
        _selectedMasterListItem.setValueQuietly(masterList[index])
        _selectedMasterListItemIndex.notifyQuietValue()
        _selectedMasterListItem.notifyQuietValue()
        _detailedItem.value =
            if (selectedMasterListItem.value != null) getDetailedItem(selectedMasterListItem.value!!) else null
    }

    /**
     * Returns the detailed item associated to the passed list item
     */
    protected abstract suspend fun getDetailedItem(masterListItem: MasterItemType): DetailItemType?


    /*******************************************************************/
    /************************ Other methods ****************************/
    /*******************************************************************/

    /**
     * Modifies the master list based on the query
     */
    protected abstract suspend fun onFilterMasterListByQuery(query: String)


    /**********************************************/
    /***************** Testing ********************/
    /**********************************************/

    open val test = Test()

    open inner class Test {

        fun setSelectedMasterListItem(position: Int?) {
            _selectedMasterListItemIndex.setValueQuietly(position)
            _selectedMasterListItem.setValueQuietly(position?.let { masterList[position] })
            _selectedMasterListItemIndex.notifyQuietValue()
            _selectedMasterListItem.notifyQuietValue()
        }

        fun setDetailedItem(item: DetailItemType) {
            _detailedItem.value = item
        }
    }
}